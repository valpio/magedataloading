import pysftp
import os
from datetime import date



def saveData(master_path, current_date):
    host_name = "s-5bd0c837c63242888.server.transfer.ap-southeast-2.amazonaws.com"
    user_name = "deuser"
    key_path = "/home/coder/.ssh/test.pem"

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None

    os.mkdir(master_path + current_date)

    sftp = pysftp.Connection(host=host_name, username=user_name, private_key=key_path, cnopts=cnopts)

    for file in sftp.listdir("/"):
        print(file)
        sftp.get(remotepath="/" + file, localpath=master_path + current_date + file)

