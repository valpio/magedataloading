import pandas as pd
import uuid

from datetime import date
from os import listdir
from os.path import isfile, join
from datetime import datetime

from StoreLocalData import saveData
from ImportData import import_table


if __name__ == '__main__':
    batch_id = str(uuid.uuid1())
    master_path = "/home/coder/sample_data/Magellan/"
    current_date = date.today().strftime("%y-%m-%d") + "/"
    saveData(master_path, current_date)
    file_names = [f for f in listdir(master_path + current_date) if isfile(join(master_path + current_date, f))]

    start_time = str(datetime.now())
    for i in file_names:
        dataframe = pd.read_csv(master_path + current_date + i, sep='\t', skipfooter=1, engine='python')
        dataframe['source_file'] = i
        dataframe['batch_id'] = batch_id
        if i.lower().endswith(".sdc"):
            import_table(dataframe, "sdc")
        elif i.lower().endswith(".sde"):
            import_table(dataframe, "sde")
        elif i.lower().endswith(".sdl"):
            import_table(dataframe, "sdl")
        else: print("new file format!")

    end_time = str(datetime.now())

    meta_dict = { 'batch_id': [batch_id],
                'start_time': [start_time],
                'end_time': [end_time]}

    meta_df = pd.DataFrame.from_dict(meta_dict)

    import_table(meta_df, 'meta')


