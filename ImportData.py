from sqlalchemy import create_engine
# from sqlalchemy.dialects.postgresql import psycopg2

def import_table(dataframe, table_name):
    database_type = "postgresql"
    host_name = "quantdb.cunnhwew5qnq.ap-southeast-2.rds.amazonaws.com"
    port = "5432"
    user_name = "postgres"
    password = ""
    database_name = "magellan"
    schema_name = "landing"

    engine = create_engine(database_type+"://"+user_name+":"+password+"@"+host_name+":"+port+"/"+database_name,
                           connect_args={'options': '-csearch_path={}'.format(schema_name)})
    dataframe.to_sql(table_name+"_landing", engine, if_exists='append')
    print("dataloaded")
